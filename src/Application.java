import math.jabri.Polynomial;
import java.util.Scanner;
/**
 * <h2>Application Main</h2>
 * the main function
 * 
 * */


public class Application{
	public static void main(String[] args){

		Scanner reader = new Scanner(System.in);
		String first,second;
		System.out.println("Welcome ");
		System.out.println("Enter data input ");

		
		Polynomial myobj = new Polynomial();
		Polynomial myobj2 = new Polynomial();
		
		first = reader.nextLine();
		myobj.add(first);
		while(true){
			
			char menu = reader.nextLine().charAt(0);
			switch(menu){
				case '+':
					second = reader.nextLine();
					myobj2.add(second);
					myobj.simplify();
					myobj2.simplify();
					myobj.addition(myobj2);
					System.out.println(myobj);
					break;
				case '-':
					second = reader.nextLine();
					myobj2.add(second);
					myobj.simplify();
					myobj2.simplify();
					myobj.substraction(myobj2);
					System.out.println(myobj);
					break;
					//substraction should be done
				case '*':
					second = reader.nextLine();
					myobj2.add(second);
					myobj.simplify();
					myobj2.simplify();
					myobj.multipication(myobj2);
					System.out.println(myobj);
					break;
					//multipication should be done
				case '/':
					second = reader.nextLine();
					myobj2.add(second);
					myobj.simplify();
					myobj2.simplify();
					if(myobj.checkDivisionPossibility(myobj2) == false)
						System.out.println("Division is not possible");
					else myobj.divide(myobj2);
					
					System.out.println(myobj);
					break;
					//multipication should be done				
				case '#':
					myobj.simplify();
					System.out.println(myobj);
					
					System.exit(0);
					break;
				default:
				    System.exit(0);
				
				
			}
		}
	}
	 
	private static void printMenu(){
		System.out.println("Enter The Number You Want ");
		System.out.println("1 - Addition ");
		System.out.println("2 - Substraction ");
		System.out.println("3 - Multipication ");
		System.out.println("4 - Division ");
		System.out.println("# - Exit ");
	}
	
	
}
