package math.jabri;
public class Phrase{
	private double factor;
	private Item item;
	private int size;
	private Phrase next;
	
	public Phrase(String text){
		next = null;
		item = null;
		add(text);

	}
	public Phrase(){
		next = null;
		item = null;
	}
	
	/**
	 * the add function process the sub sentence between + and -
	 */
	public boolean add(String text){
		int i = 0;
		for(i = 0;i<text.length();i++){
			if(text.charAt(i) == '(' || i == text.length()-1){
				String factor_text = text.substring(0,i);
				if (i == text.length()-1) {//if it is an integer
					
					factor = Double.parseDouble(text.substring(0,i+1));
					//System.out.println("integer" + factor);
				}	
				else if(factor_text.equals("-"))
					factor = -1;
				else if (factor_text.equals("+")){
					factor = 1; 
				}

				else if (i == 0)//when we have smth like this (x^2)+2(y^3) **exception appears at first
					factor = 1;
				else{ // when we have smth like this 1 2 3 
					factor = Double.parseDouble(factor_text);
				}
			  break;
			}
		}
		if(factor == 0){
			System.out.println("here false");
			return false;
		}
		//System.out.println(text.length());
		//System.out.println(text);
			
		item = new Item();
		Item move = item;
		boolean first = true;
		for(int j = i;j<text.length();j++){
			if(text.charAt(j)==')'){
				if(first == true){
					boolean result = move.add(text.substring(i,j+1));
					if(result == true){
						size++;
						first = false;
					}
					
				}else{
					Item newIt = new Item();
					boolean result = newIt.add(text.substring(i,j+1));
					if(result == true){
						size ++;
						move.setNext(newIt);
						move = move.getNext();
					}
				}
				i = j+1;
			}
		}
		if(size == 0)
		  item = null;
		
		return true;
	}
	
	public void addToEnd(char alpha,int power){
		
		Item move = item;
		if(move == null){
			item = new Item(alpha,power);
			size++;
			
		}else{
			for(;move.getNext()!=null;move=move.getNext());
			move.setNext(new Item(alpha,power));
			size++;
		}
	}
	
	public void addOptimize(char alpha,int power){
		Item move = item;
		boolean add = false;
		for(;move!=null;move=move.getNext()){
			if(move.getAlpha() == alpha){
				move.setPower(move.getPower()+power);
				add = true;
			}
		}
		if(add == false){
			addToEnd(alpha,power);
		}
		
	}
		
	public static Phrase multiplyOptimize(Phrase A,Phrase B){
		Phrase temp = new Phrase();
		Phrase thead = temp;
		temp.factor = A.factor * B.factor;
		Item moveA = A.item;
		for(;moveA!=null;moveA = moveA.next){
			temp.addToEnd(moveA.getAlpha(),moveA.getPower());
		}
		moveA = temp.item;
		int i = 0;
		int si = temp.getSize();
		for(;i<si;moveA = moveA.getNext()){
			Item move = B.item;
			boolean add = false;
			
			for(;move!=null;move=move.getNext()){
				if(move.getAlpha() == moveA.getAlpha()){
					moveA.setPower(moveA.getPower()+move.getPower());
					break;
				}
			}

			i++;
		}
		for(Item move=B.item;move!=null;move=move.getNext()){
			if(temp.checkCharExistance(move.getAlpha()) == false){
				temp.addToEnd(move.getAlpha(),move.getPower());
			}
			
		}
		return temp;
	}
	public boolean addition(Phrase O){
		if(checkSimplify(O)){
			factor += O.getFactor();
			return true;
		}
		return false;
	}
	public boolean substraction(Phrase O){
		if(checkSimplify(O)){
			factor -= O.getFactor();
			return true;
		}
		return false;
	}
	public void simplify(){
		Item move = item;
		if(move != null){
			for(;move.getNext()!=null;move=move.getNext()){
					if(move.getNext().getPower() == 0){
						move.setNext(move.getNext());
					}
				move = move.getNext();
			}
			if(item.getPower() == 0){
				item.setNext(item.getNext());
			}
		}
	}
	public boolean checkSimplify(Phrase o){
		if (o == null){
		  return false;
	     }
		if(o.size != size){
			return false;
			
		}

		Item move = item;
		Item moveO = o.item;

		for(;move!=null;move=move.getNext()){
			boolean check = false;
			for(moveO = o.item;moveO!=null;moveO=moveO.getNext()){
				if(move.getAlpha() == moveO.getAlpha()){
					if(move.getPower() != moveO.getPower()){
						return false;
					}
					check = true;
				}
			}
			if(check == false) return false;
			
			
		}
		
		return true;
	}
	public double getFactor(){
		return factor;
	}
	public Item getItem(){
		return item;
	}
	public Phrase getNext(){
		return next;
	}
	public int getSize(){
		return size;
	}
	public void addOneToSize(){
		size++;
	}
	public void setFactor(double f){
		factor = f;
	}
	public void setItem(Item i){
		item = i;
	}
	public void setNext(Phrase n){
		next = n;
	}
	public Polynomial divide(Phrase divider){
		double q = factor/divider.factor;
		Phrase newObj = new Phrase();
		newObj.setFactor(q);
		Item newItem = new Item();
		Item mItem = newItem;
		Item move = item;
		int s = 0;
		for(;move!=null;move=move.getNext()){
			if(divider.checkCharExistance(move.getAlpha()) == true){
				if(move.getPower() != divider.findCharPower(move.getAlpha())){
					mItem.setAlphabet(move.getAlpha());
					mItem.setPower(move.getPower()-divider.findCharPower(move.getAlpha()));
					mItem.setNext(new Item());
					mItem = mItem.getNext();
					s++;
				}
				
			}else{
				//age nabood bayad ba hamin tavan ezafe shavad
				mItem.setAlphabet(move.getAlpha());
				mItem.setPower(move.getPower());
				mItem.setNext(new Item());
				mItem = mItem.getNext();	
				s++;			
			}
		}
		newObj.size = s;
		if(s==0)newItem =null;
		else if(s==1)newItem.setNext(null);
		else{
			for(mItem=newItem;mItem.getNext().getNext()!=null;mItem=mItem.getNext());
			newItem.getNext().setNext(null);
		}
		newObj.setItem(newItem);
		return new Polynomial(newObj);
	}
	
	public boolean checkCharExistance(char a){
		Item move = item;
		for(;move!=null;move=move.getNext()){
			if(move.getAlpha() == a)
				return true;
		}
		return false;
	}

	public int findCharPower(char a){
		Item move = item;
		for(;move!=null;move=move.getNext()){
			if(move.getAlpha() == a)
				return move.getPower();
		}
		return -999; //we should throw exception
	}
	public int findMaxPower(){
		if(item == null)return 0;//this might create bug
		int max = item.getPower();
		Item move = item.getNext();
		for(;move!=null;move=move.getNext()){
			if(move.getPower()>max){
				max = move.getPower();
			}
		}
		return max;
	}
	public String toString(){
		
		String out;
		if(factor>0)
			out="+"+factor;
		///else if (factor == 0)
		 //   return "";
		else out=factor+""; 
		Item move = item;
		if(move == null) return out;
		for(;move!=null;move=move.getNext()){
			if(move.getPower()!=0 && move!=null)
			  out +=move.toString();
		}
		return out;
	}
	
	
}
