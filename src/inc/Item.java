package math.jabri;
public class Item{
	
	private char alphabet;
	private int power;
	Item next;
	
	public Item(){
		next = null;
	}
	public Item(char c,int p){
		alphabet = c;
		power = p;
		next = null;
	}
	
	/**
	 * this function process (x^2) or (x) and seprate alphabet and power
	 */ 
	public boolean add(String text){
		int i =0;
		for(i=0;i<text.length() && text.charAt(i) !='^';i++);
		if(i == text.length()){
			power = 1;
			alphabet = text.charAt(1);
		}
		else{
			alphabet = text.charAt(i-1);
			power = Integer.parseInt(text.substring(i+1,text.length()-1));
		}

		if(power == 0)
		  return false;
		else return true;
	}
	
	public void addToEnd(char a,int p){
		Item move = next;
		for(;move.next!=null;move=move.next);
		move.next = new Item(a,p);
	}
	
	
	public char getAlpha(){
		return alphabet;
	}
	public int getPower(){
		return power;
	}
	public Item getNext(){
		return next;
	}
	
	
	
	public void setNext(Item newItem){
		next = newItem;
	}
	public void setPower(int p){
		power = p;
	}
	public void setAlphabet(char al){
		alphabet = al;
	}
	public String toString(){
		return "("+alphabet+"^"+power+")";
	}
	
	
	
	
}
