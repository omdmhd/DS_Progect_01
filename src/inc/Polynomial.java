package math.jabri;
/**
 *<h3>Polymonial </h3>
 * 
 * @author Omid Mohammadi
 * @version 1.0 
 *
 */

public class Polynomial{

	
	private Phrase head1;//main linked list

	public Polynomial(Phrase h){
		head1 = h;
	}
	public Polynomial(){
		head1 = null;
	}
	/**
	 * this function will get the complete sentence 
	 * and divide it to sub sentence and will pass it Phrase add function
	 * to process the rest of the text 
	*/
	
	public void add(String text){
		
		int f = 0;
		head1 = new Phrase();
		Phrase move = head1;
		int i = 0;
		boolean first = true;
		for(i=0;i<text.length();i++){
			if(text.charAt(i) == '+' || text.charAt(i) == '-'){
				if (first == true){
					boolean result = move.add(text.substring(f,i));
					//System.out.println(text.substring(f,i));
					f = i; 
					//if we have a sentence like -2(x^2) first it will be ignored and then in the second time it will be proccessed.
					if(result == true){
						first = false;
					}					
				}else{
					Phrase newI = new Phrase();
					boolean result = newI.add(text.substring(f,i));
					//System.out.println(text.substring(f,i));

					//System.out.println("Poltnomial : "+text.substring(f,i));
					f = i;
					if(result == true){
						move.setNext(newI);
						move = move.getNext();
					}
				}
			}
		}
		
		
		// we add the last sentence with this code
		if (first == true){
					
			boolean result = move.add(text.substring(f,i));
			//System.out.println(text.substring(f,i));
			f = i;
			if(result == true){
			    first = false;
			}					
		}else{
			Phrase newI = new Phrase();
			boolean result = newI.add(text.substring(f,i));
			//System.out.println(text.substring(f,i));
			//System.out.println("Poltnomial : "+text.substring(f,i));
			f = i;
			if(result == true){
				move.setNext(newI);
				move = move.getNext();
			}
		}
		

	}
	public void addToEnd(Phrase O){
		if(head1 == null){
			head1 = O;
		}
		Phrase move = head1;
		for(;move.getNext()!=null;move = move.getNext());
		move.setNext(O);
		move = move.getNext();
	}
	public Phrase addition(Polynomial o){
		simplify();
		o.simplify();
		Phrase move,moveO;
		Phrase temp = new Phrase();
		Phrase htemp = temp;
		for(moveO=o.head1;moveO!=null;moveO=moveO.getNext()){
			boolean add = false;
			for(move=head1;move!=null;move=move.getNext()){
			
			
				if(move.addition(moveO) == true){
					add = true;
					break;
				}
			}
			if(add == false){
				//it doesn't have size to care about
				temp.setNext(moveO);
				temp = temp.getNext();
			}
		}
		addToEnd(htemp.getNext());
		simplify();
		return head1;
	
	/*	simplify();
		o.simplify();
		Phrase move = head1;
		Phrase moveO = o.head1;
		Phrase tail = new Phrase();
		Phrase backup = tail;
		for(;moveO!=null;moveO = moveO.getNext()){
			boolean check = false;
			for(move=head1;move!=null;move=move.getNext()){
				if(moveO.checkSimplify(move)){
					move.setFactor(move.getFactor()+moveO.getFactor());
					
					check = true;
					break;
				}
			}
			if(check == false){
				Phrase newI = new Phrase();
				newI.setFactor(moveO.getFactor());
				newI.setItem(moveO.getItem());
				tail.setNext(newI);
				tail = tail.getNext();
			}
		}
		for(move=head1;move.getNext()!=null;move=move.getNext());
		move.setNext(backup.getNext());
		simplify();
		return head1;*/
	} 
	public Phrase substraction(Polynomial o){
		
		simplify();
		o.simplify();
		Phrase move,moveO;
		Phrase temp = new Phrase();
		Phrase htemp = temp;
		for(moveO=o.head1;moveO!=null;moveO=moveO.getNext()){
			boolean add = false;
			for(move=head1;move!=null;move=move.getNext()){
			
			
				if(move.substraction(moveO) == true){
					add = true;
					break;
				}
			}
			if(add == false){
				//it doesn't have size to care about
				temp.setNext(moveO);
				temp = temp.getNext();
			}
		}
		for(temp = htemp;temp!=null;temp=temp.getNext())
			temp.setFactor(temp.getFactor()*(-1));
		addToEnd(htemp.getNext());
		simplify();
		return head1;
		/*Phrase move = o.head1;
		for(;move!=null;move = move.getNext()){
			move.setFactor(move.getFactor()*(-1));
		}
		addition(o);
		simplify();*/
		//return head1;
	}
	
	public Phrase multipication(Polynomial o){
		
		simplify();
		o.simplify();
		Phrase all = new Phrase();
		Phrase thead = all;
		Phrase move1,move2;
		for(move1=head1;move1!=null;move1=move1.getNext()){
			for(move2=o.head1;move2!=null;move2=move2.getNext()){
				Phrase temp = Phrase.multiplyOptimize(move1,move2);
				//System.out.println(temp);
				all.setNext(temp);
				all = all.getNext();
				
			}
		}
		simplify();
		head1 = thead.getNext();
		return head1;
		
		/*Phrase head = new Phrase();
		Phrase back = head;
		boolean first_h = true;
		for(move1=head1;move1!=null;move1=move1.getNext()){
			for(move2=o.head1;move2!=null;move2=move2.getNext()){
				double fac = move1.getFactor()*move2.getFactor();
				boolean first = true;
				Item newItem = new Item();
				Item backNewItem = newItem;
				Item mitem1 ,mitem2 = move1.getItem();
				for(mitem1 = move2.getItem();mitem1!=null;mitem1=mitem1.getNext()){
					int p;
					char c;
					if(move1.checkCharExistance(mitem1.getAlpha()) == true){
						 p = mitem1.getPower() + move2.findCharPower(mitem1.getAlpha());
						 c = mitem1.getAlpha();
					}else{
						p = mitem1.getPower();
						c = mitem1.getAlpha();
					}
					if(first == true){
						
						newItem.setAlphabet(c);
						newItem.setPower(p);
						first = false;
					}else{
						newItem.setNext(new Item());
						newItem = newItem.getNext();
						newItem.setAlphabet(c);
						newItem.setPower(p);						
							
					}
					
				}
				for(mitem2 = move1.getItem();mitem2!=null;mitem2=mitem2.getNext()){
					
					if(move2.checkCharExistance(mitem2.getAlpha()) == false){

						newItem.setNext(new Item());
						newItem = newItem.getNext();
						newItem.setAlphabet(mitem2.getAlpha());
						newItem.setPower(mitem2.getPower());							
												
					}
					
				}
				if(first_h == true){
					head.setFactor(fac);
					head.setItem(backNewItem);
					first_h = false;
					
				}else{

					head.setNext(new Phrase());
					head = head.getNext();
					head.setFactor(fac);
					head.setItem(backNewItem);
					first_h = false;					
					
				}
				
				
				
				
			}
			
		}
		head1 = back;
		simplify();*/
	}
	
	public void divide(Polynomial divisor){
		divisor.sortPhrase();
		sortPhrase();
		System.out.println(this);
		Phrase myPh = new Phrase();
		Phrase back = myPh;
		while(  head1!=null && (divisor.head1.findMaxPower()<=head1.findMaxPower() || divisor.head1.checkSimplify(head1) == true ) && checkDivisionPossibility(divisor) == true ){
			Polynomial out = head1.divide(divisor.head1);
			//System.out.println(out);
			myPh.setNext(out.head1);
			myPh = myPh.getNext();
			out.multipication(divisor);
			substraction(out);
			sortPhrase();
					
		}
		System.out.println("BAGHI Mande:: " +this.toString());
		head1 = back.getNext();
	}
	public void sortPhrase(){
		Phrase sortedHead = new Phrase();
		Phrase thead = sortedHead;
		Phrase move1 = head1;
		for(;move1!=null;){

			//System.out.println("head1 : " +  move1);
			Phrase max = move1;
			boolean next = true;
			for(Phrase move2 = head1;move2!=null;move2=move2.getNext()){
				//System.out.println("is "+move2+ " biger that "+max+"  ?");
				if(isBiger(move2,max) == true){
					max = move2;
					next= false;
					move1 = head1;
					//System.out.println("yes" + move2 + "   "+ max);
				}
			}
			//System.out.println("mx "  +max);
			//max.setNext(null);
			sortedHead.setNext(max);
			removePhrase(max);
			//if(sortedHead.getNext() == null)sortedHead.getNext().setNext(null);
			sortedHead = sortedHead.getNext();
			if(next==true)
			move1=move1.getNext();
		}
		//sortedHead.setNext(null);
		
		head1 = thead.getNext();
		
	}

	public static boolean isBiger(Phrase A,Phrase B){ //when A > B
		if(A.findMaxPower() > B.findMaxPower()){
			return true;
		}else if (A.findMaxPower() < B.findMaxPower()){
			return false;
		}else{
			if(A.getSize()>B.getSize())return true;
			else return false;
		}
		
	}
	public boolean checkDivisionPossibility(Polynomial O){
		for(Phrase move = O.head1;move!=null;move=move.getNext()){
			for(Item m = move.getItem();m!=null;m = m.getNext()){
				if(head1.checkCharExistance(m.getAlpha()) == false)
						return false;
			
				
			}
			
		}
		return true;
	}

	public void simplify(){
	/*	Phrase f_move, s_move,d_move ;
		//System.out.println(head1);
		d_move = head1;
		while(d_move!=null && d_move.getNext()!=null){
			if(d_move.getNext().getFactor() == 0){
				d_move.setNext(d_move.getNext().getNext());
			}
			d_move = d_move.getNext();
		}
		if(head1!=null && head1.getFactor() == 0){
			head1 = head1.getNext();
			
		}
		f_move = head1;
		//System.out.println(this);
		while(f_move!=null){
			s_move = f_move;
			//s_move = head1;
			//System.out.println("this item : "+f_move+" checking with");
			while(s_move!=null){
				//System.out.println("this one : "+s_move.getNext());
				//System.out.println(s_move.getNext() + "   and    "+f_move);
				if(f_move.checkSimplify(s_move.getNext()) == true){
					//System.out.println("True");
					f_move.setFactor(f_move.getFactor() + s_move.getNext().getFactor());
					s_move.setNext( s_move.getNext().getNext());
					
				}else{
					//System.out.println("False");
									}
				s_move = s_move.getNext();
			}
			f_move = f_move.getNext();
		}
		*/
		
		Phrase f_move, s_move,d_move ;
		//System.out.println(head1);
		d_move = head1;
		while(d_move!=null && d_move.getNext()!=null){
			if(d_move.getNext().getFactor() == 0){
				d_move.setNext(d_move.getNext().getNext());
			}
			d_move = d_move.getNext();
		}
		if(head1!=null && head1.getFactor() == 0){
			head1 = head1.getNext();
			
		}
		f_move = head1;
		while(f_move!=null){
			s_move = f_move.getNext();
			//s_move = head1;
			while(s_move!=null){
				//System.out.println(s_move.getNext() + "   and    "+f_move);
				if(f_move.checkSimplify(s_move) == true){
					f_move.setFactor(f_move.getFactor() + s_move.getFactor());
					Phrase temp = s_move;
					s_move = s_move.getNext();
					//System.out.println("remove");
					removePhrase(temp);
					
				}else{
					s_move = s_move.getNext();
					//System.out.println("False");
									}
				
			}
			f_move = f_move.getNext();
		}
		
	}
	
	private boolean removePhrase(Phrase r){
		if(head1 == r){
			head1 = head1.getNext();
			//System.out.println("delete");

			return true;
		}
		
		if(head1 == null) return false;
		if(head1.getNext() == null) return false;
		Phrase move = head1;
		while(move.getNext()!=null){
			//System.out.println("hey");
			if(move.getNext() ==  r){
				move.setNext(move.getNext().getNext());
				//System.out.println("delete");
				break;
			}
			if(move.getNext() != null )move=move.getNext();
			else break;
		}
		return true;
		
	}

	public String toString(){
		String out ="";
		Phrase move = head1;
		if (move == null) return "0";
		for(;move!=null;move=move.getNext())
			out+=move.toString();
		
		return out;
	}
	
}
